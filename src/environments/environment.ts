// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDWMMe7A28qwRO87EC0Gm0Hle-Cs3X_vBU",
    authDomain: "product-firebase-ef4bb.firebaseapp.com",
    databaseURL: "https://product-firebase-ef4bb.firebaseio.com",
    projectId: "product-firebase-ef4bb",
    storageBucket: "product-firebase-ef4bb.appspot.com",
    messagingSenderId: "1066658496158",
    appId: "1:1066658496158:web:b67b44358d80b7ffa3458f",
    measurementId: "G-304RQX6TGX"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
