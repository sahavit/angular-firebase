import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument} from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { Product } from './models/product';
import { map, finalize } from 'rxjs/operators';
import { AngularFireAuth } from '@angular/fire/auth';
import { User } from './models/user';
import { Router } from '@angular/router';
import { AngularFireFunctions} from '@angular/fire/functions';
import { AngularFireStorage } from '@angular/fire/storage';

// import * as firebase from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class FirebaseDBService { 

  products: Observable<Product[]>
  prodColl: AngularFirestoreCollection<Product>;
  prodDoc: AngularFirestoreDocument<Product>;

  regErr:string;
  loginErr:string;
  upError:string;
  addError:string;
  delError:string;
  userData: any;
  res:any;

  // private basePath:string = '/uploads';
  // private uploadTask: firebase.storage.UploadTask;

  constructor(private firestore: AngularFirestore,
    private router:Router,private afuth: AngularFireAuth, 
    private firefunc: AngularFireFunctions, private storage:AngularFireStorage) { 
    /* Saving user data in localstorage when 
    logged in and setting up null when logged out */
    this.afuth.authState.subscribe(user => {
      if (user) {
        this.userData = user;
        sessionStorage.setItem('user', JSON.stringify(this.userData));
        JSON.parse(sessionStorage.getItem('user'));
      } else {
        sessionStorage.setItem('user', null);
        JSON.parse(sessionStorage.getItem('user'));
      }
    })
  }

  imgUpload(img, formValue, prod:Product){
    var filePath = `images/${this.userData.uid}/${img.name}`;
    const fileRef = this.storage.ref(filePath);
    this.storage.upload(filePath, img).snapshotChanges().pipe(
      finalize(() => {
        fileRef.getDownloadURL().subscribe((url) => {
          formValue['img'] = url;
          prod.imgUrl = url;
          this.updateProd(prod);
        })
      })
    ).subscribe();
    
  }



  // pushUpload(upload: Upload){
  //     let storageRef = firebase.storage().ref();
  //     this.uploadTask = storageRef.child(`${this.basePath}/${this.userData.uid}/${upload.file.name}`).put(upload.file);

  //     this.uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
  //       (snapshot) => {
  //         upload.progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
  //       },
  //       (error) => {
  //         console.log(error)
  //       },
  //       () => {
  //         upload.url = this.uploadTask.snapshot.downloadURL;
  //         upload.name = upload.file.name;
  //         // this.saveFileData(upload);
  //       }
  //       )
  // }

  async callCloudFunc(){
    let data = { pid: 1000, name: "Radio"}
    const pidFilter = this.firefunc.httpsCallable('pidFilter');
    pidFilter(data).subscribe( (result) => {
      console.log(result)
    })
    
    }
    

    // callCloudFunc1(){
    //   let data = { name: "Sam"}
    // const hello = this.firefunc.httpsCallable('hello');
    // hello(data).subscribe( (result) => {
    //   console.log(result)
    // })}

  get isLoggedIn(): boolean {
    const user = JSON.parse(sessionStorage.getItem('user'));
    return (user !== null) ? true : false;
  }

  viewProducts(): Observable<Product[]> {
    // return this.prodColl.valueChanges.();
    return this.products; 
  }

  addProduct(prod:Product){
    this.prodColl.add(prod).catch((error) =>{
      this.addError=error.message;
    });
    return this.addError;
  }

  deleteProd(prod:Product){
    this.prodDoc = this.prodColl.doc(`${prod.id}`)
    this.prodDoc.delete().catch((error) =>{
      this.delError=error.message;
    });
    return this.delError;
  }

  updateProd(prod:Product){
    this.prodDoc = this.prodColl.doc(`${prod.id}`)
    this.prodDoc.update(prod).catch((error) =>{
      this.upError=error.message;
    });
    return this.upError;
  }

  signUp(u:User){
    return this.afuth.createUserWithEmailAndPassword(u.email,u.password)
    .then((result) => {
      this.SetUserData(result.user);
      this.router.navigate(['home']);
    }).catch((error) => {
      this.regErr=error.message
      window.alert(error);
    });
  }

  loginError():boolean{
    if(this.loginErr)
      return true;
  }

  signIn(u:User) {
    return this.afuth.signInWithEmailAndPassword(u.email,u.password)
      .then((result) => {
        this.router.navigate(['home'])
        console.log(result.user.uid);
        this.SetUserData(result.user);
      }).catch((error) => {
        this.loginErr = error.message
        window.alert(error)
      })
  }

  signOut() {
    return this.afuth.signOut().then(() => {
      sessionStorage.removeItem('user');
      this.router.navigate(['login']);
    })
  }

  SetUserData(user){  
    this.prodColl=this.firestore.collection(`users/${user.uid}/products`, ref => ref.orderBy('pid','asc'));
    this.products = this.prodColl.snapshotChanges().pipe(map(changes => {
      return changes.map(a => {
        const data = a.payload.doc.data() as Product;
        data.id = a.payload.doc.id;
        return data;

      })
    }))
  }
}
