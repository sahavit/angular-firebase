import { Component, OnInit } from '@angular/core';
import { FirebaseDBService } from 'src/app/firebase-db.service';
import { Product } from 'src/app/models/product';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {

  // product = 
  // [{pid: 1001, name:"Phone", price: 8500, quantity: 10},
  //   {pid:1002, name:"Bat", price: 700, quantity: 7},
  //   {pid:1003, name:"Ball", price: 630, quantity: 5},
  //   {pid:1004, name:"TShirt", price: 1200, quantity: 15},
  //   {pid:1005, name:"Cap", price: 250, quantity: 25}]

  products:Product[] = [];

  constructor(private fireservice:FirebaseDBService) { }

  ngOnInit(): void {
    this.getAllProducts();
  }

  getAllProducts(){
    this.fireservice.viewProducts().subscribe( products => {
      this.products=products;
    })
  }

}
