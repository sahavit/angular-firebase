import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/models/product';
import { FirebaseDBService } from 'src/app/firebase-db.service';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';


@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {

  addProduct:Product = {
    pid: 0,
    name: '',
    price: 0,
    quantity: 0
  };

  show=true;
  success = false;
  fail = false;
  selectedImg:any = null;

  productAddForm:FormGroup;
  constructor(private fireservice:FirebaseDBService,private fb: FormBuilder) { }

  ngOnInit(): void {
    this.createForm();
  }

  addProd(){
    this.addProduct.pid=this.productAddForm.value['pid'];
    this.addProduct.name=this.productAddForm.value['name'];
    this.addProduct.price=this.productAddForm.value['price'];
    this.addProduct.quantity=this.productAddForm.value['quantity'];
    // this.fireservice.imgUpload(this.selectedImg, this.productAddForm, this.addProduct);
    if(this.addProduct.pid!==null){
      if(this.fireservice.addProduct(this.addProduct)==null){
        this.show=false;
        this.success=true;
        this.fail=false;
      }
      else{
        this.show=false;
        this.fail=true;
        this.success=false;
      }
    }
  }


  changeS(){
    this.show=true;
    this.success=false;
  }

  changeF(){
    this.show=true;
    this.fail=false;
  }

  createForm() {
    this.productAddForm = this.fb.group({
      pid: new FormControl("", [Validators.required, Validators.min(1000)]),
      name: ["", Validators.required],
      price: ["", [Validators.required, Validators.min(1)]],
      quantity: ["", [Validators.required, Validators.min(1)]],
      img: [""]
    });
  }

  showPreview(event:any){
    if(event.target.files && event.target.files[0]){
      const reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      this.selectedImg = event.target.files[0];
    }
    else{
      this.selectedImg=null;
    }
  }


}
