import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/models/product';
import { FirebaseDBService } from 'src/app/firebase-db.service';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  // product = 
  // [{pid: 1001, name:"Phone", price: 8500, quantity: 10},
  //   {pid:1002, name:"Bat", price: 700, quantity: 7},
  //   {pid:1003, name:"Ball", price: 630, quantity: 5},
  //   {pid:1004, name:"TShirt", price: 1200, quantity: 15},
  //   {pid:1005, name:"Cap", price: 250, quantity: 25}]

  products:Product[] = [];
  // products:Observable<any>;

    update = false;
    success = false;
    fail = false;
    upProduct:Product = {
      pid: 0,
      name: '',
      price: 0,
      quantity: 0
    };
    msg:string;
    
  constructor(private fireservice:FirebaseDBService,private router:Router) { }

  ngOnInit(): void {
    this.getAllProducts();
    // this.products = this.fireservice.viewProducts();
  }

  getAllProducts(){
    this.fireservice.viewProducts().subscribe( products => {
      this.products=products;
    })
  }

  setProduct(prod:Product){
      this.upProduct.id=prod.id;
      this.upProduct.pid=prod.pid;
      this.upProduct.name=prod.name;
      this.upProduct.price=prod.price;
      this.upProduct.quantity=prod.quantity;
      this.update=true;
  }

  updateProd(){
    // this.upProduct.id=prod.id;
    if(this.fireservice.updateProd(this.upProduct)==null){
      this.update=false;
      this.success=true;
      this.fail=false;
    }
    else{
      this.update=false;
      this.fail=true;
      this.success=false;
    }
  }


}
