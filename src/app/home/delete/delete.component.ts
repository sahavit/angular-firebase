import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/models/product';
import { FirebaseDBService } from 'src/app/firebase-db.service';

@Component({
  selector: 'app-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.css']
})
export class DeleteComponent implements OnInit {

  delete:boolean=true;
  confirm:boolean=false;
  success= false;
  fail = false;
  deleProd:Product = {
    pid: 0,
    name: '',
    price: 0,
    quantity: 0
  }
  products:Product[] = [];

  // product = 
  // [{pid: 1001, name:"Phone", price: 8500, quantity: 10},
  //   {pid:1002, name:"Bat", price: 700, quantity: 7},
  //   {pid:1003, name:"Ball", price: 630, quantity: 5},
  //   {pid:1004, name:"TShirt", price: 1200, quantity: 15},
  //   {pid:1005, name:"Cap", price: 250, quantity: 25}]

  constructor(private fireservice:FirebaseDBService) { }

  ngOnInit(): void {
    this.getAllProducts();
  }

  getAllProducts(){
    this.fireservice.viewProducts().subscribe( products => {
      this.products=products;
    })
  }

  confirmDel(prod:Product){
    // this.fireservice.deleteProd(this.deleProd);
    let ok=confirm("Are you sure you want to delete this Product '" +prod.name +"' ?");
    if(ok){
      if(this.fireservice.deleteProd(prod)==null){
        this.delete=false;
        this.success=true;
        this.fail=false;
      }
      else{
        this.delete=false;
        this.fail=true;
        this.success=false;
      }
    }
      
  }

  changeS(){
    this.delete=true;
    this.success=false;
  }

  changeF(){
    this.delete=true;
    this.fail=false;
  }

}
