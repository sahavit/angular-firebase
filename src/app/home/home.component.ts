import { Component, OnInit } from '@angular/core';
import { Product } from '../models/product';
import { Observable } from 'rxjs';
import { FirebaseDBService } from '../firebase-db.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  // product = 
  // [{pid: 1001, name:"Phone", price: 8500, quantity: 10},
  //   {pid:1002, name:"Bat", price: 700, quantity: 7},
  //   {pid:1003, name:"Ball", price: 630, quantity: 5},
  //   {pid:1004, name:"TShirt", price: 1200, quantity: 15},
  //   {pid:1005, name:"Cap", price: 250, quantity: 25}]


  products: Observable<any[]>;
  viewDetails:boolean = true;
  constructor(private fireservice:FirebaseDBService) {

 }

  ngOnInit(){
    this.products = this.fireservice.viewProducts();
  }

  callFunc(){
    this.fireservice.callCloudFunc();
  }

  logout(){
    this.fireservice.signOut();
  }

}
