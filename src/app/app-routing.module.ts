import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component'
import { HomeComponent } from './home/home.component';
import { ViewComponent } from './home/view/view.component';
import { CreateComponent } from './home/create/create.component';
import { EditComponent } from './home/edit/edit.component';
import { DeleteComponent } from './home/delete/delete.component';


const routes: Routes = [
  {path:'login',component:LoginComponent},
  {path: '', redirectTo: 'login', pathMatch: 'full' },
  {path:'home',component:HomeComponent, children: [
    {path:'view',component:ViewComponent},
    {path:'',redirectTo: 'view', pathMatch: 'full'},
    {path:'create',component:CreateComponent},
    {path:'edit',component:EditComponent},
    {path:'del',component:DeleteComponent},
  ]}
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
