import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FirebaseDBService } from '../firebase-db.service';
import { User } from '../models/user';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  signin:boolean=true;
  vectorimg= "assets/images/vec2.jpg"

  loginUser:User={
    email: '',
    password: ''
  }
  regiUser:User={
    email: '',
    password: ''
  };
  email:string;
  password:string;
  emailForm1 = new FormControl('',[Validators.required]);
  emailForm2 = new FormControl('',[Validators.required]);
  tryToLogin1=false;
  tryToLogin2=false;

  constructor(private router:Router,private fireservice:FirebaseDBService) { }

  ngOnInit(): void {
  }

  goToHome(){
     this.router.navigate(['home']);
  }

  login(){
    this.tryToLogin1=true;
    this.fireservice.signIn(this.loginUser);

  }

  register(){
    this.tryToLogin2=true;
    this.fireservice.signUp(this.regiUser);
  }

}
