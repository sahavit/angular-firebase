export class Product{
    id?: string;
    pid: number;
    name:string;
    price: number;
    quantity: number;
    imgUrl?: string;
}