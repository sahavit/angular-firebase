const functions = require('firebase-functions');
const admin = require('firebase-admin')
admin.initializeApp(functions.config().firebase);

// // Create and Deploy Your First Cloud Functions
// https://firebase.google.com/docs/functions/write-firebase-functions


exports.pidFilter = functions.https.onCall(  (data, context) => {
    if(!context.auth){
        throw new functions.https.HttpsError( 
            'unauthenticated',
            'Only authenticated users can send requests'
        );
    }
    else{
        const userid = context.auth.uid;
        const prodRef = admin.firestore().collection(`users/${userid}/products`);
        const snapshot = prodRef.where('name', '==', data.name).where('pid', '==', data.pid).get(); 
          
        snapshot.forEach(doc => {
            // console.log(doc.id, '=>', doc.data.name);
            return doc;
          });
    }

})

exports.hello = functions.https.onCall((data,context) => {
    const name = data.name;
    return `Hello, ${name}`;
})
